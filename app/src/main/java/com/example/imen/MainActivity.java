package com.example.imen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MainActivity extends AppCompatActivity {
    Button connectionButton;
    Connection connect;
    String ConnectionResult="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        connectionButton = (Button) findViewById(R.id.connectionButton);

        connectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            GetTextFromSQL();
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });




    }
    public void GetTextFromSQL() throws ClassNotFoundException {
        /*TextView txt1=(TextView)findViewById(R.id.t1);
        TextView txt2=(TextView)findViewById(R.id.t2);
        try{
            ConnectionHelper connectionHelper=new ConnectionHelper();
            connect =connectionHelper.connectionclass();
            Log.e("test1", (connect != null) + "");
            if(connect!=null){
                String query ="Select * from users";
                Statement st=connect.createStatement();
                ResultSet rs=st.executeQuery(query);
                while(rs.next()){
                    txt1.setText(rs.getString(1));
                    txt2.setText(rs.getString(2));

                    Toast.makeText(this, "" + txt1.getText(), Toast.LENGTH_LONG).show();
                }
            }else{
                ConnectionResult="check connection";
            }

        }catch (Exception e){
            Toast.makeText(MainActivity.this,"Eroro",Toast.LENGTH_SHORT).show();
        }*/

        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();

            connection = DriverManager
                    .getConnection("jdbc:mysql://192.168.1.7:3306/erp_galaxy", "root", "");

            String query = "Select * from user";
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                String test1 = rs.getString(1);
                String test2 = rs.getString(2);

                Log.e("azerty1", test1);
                Log.e("azerty2", test2);
            }




            Log.e("azerty", "" + (connection == null));
            Log.e("azerty", "" + connection.toString());
        } catch (SQLException throwables) {
            Log.e("azerty", throwables.getMessage());
            throwables.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        System.out.println(connection);
    }

    }